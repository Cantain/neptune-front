<?php

namespace App\Form;

use App\Entity\ContactRequest;
use App\Form\Type\ReCaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class ContactForm extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    if ($options['action'] !== null) {
      $builder->setAction($options['action']);
    }
    if ($options['contactType'] !== null) {
      $contactType = $options['contactType'];
    }

    dump($options['type']);


    $builder->add('name', TextType::class, array(
      'attr' => array(
        'placeholder' => 'Nom*',
      ),
      'label' => false,
      'constraints' => array(
        new NotBlank()
      )
    ))

    ->add('firstname', TextType::class, array(
      'attr' => array(
        'placeholder' => 'Prénom',
      ),
      'required' => false,
      'label' => false,
    ))

    ->add('phone', TelType::class, array(
      'attr' => array(
        'placeholder' => 'Téléphone',
      ),
      'label' => false,
      'required' => false,
    ))

    ->add('email', EmailType::class, array(
      'attr' => array(
        'placeholder' => 'Email',
      ),
      'required'          => true,
      'label' => false,
      'constraints' => array(
        new Email()
      )
    ));
    if($options['type'] === 'dispo'){
      $builder
      ->add('arrive', DateType::class, array(
        'attr' => array(
          'placeholder' => "Date d'arrivée",
          'class'         =>'arrive',
        ),
        'widget'  => 'single_text',
        'html5'       => true,
        'label' => false,
        'empty_data' => '',
        'constraints' => array(
          new NotBlank()
        )
      ))

      ->add('leaving', DateType::class, array(
        'attr' => array(
          'placeholder' => "Date de départ",
          'class'         =>'leave',
        ),
        'widget'  => 'single_text',
        'html5'       => true,
        'label' => false,
        'empty_data' => '',
        'constraints' => array(
          new NotBlank()
        )          
      ))
      ->add('hebergement', ChoiceType::class, [
        'attr' => array(
          'placeholder' => "Hébergement",
        ),
        'choices'  => [
          'Hébergement' => null,
          '2 personnes' => '2 personnes',
          '4 personnes' => '4 personnes',
          '8 personnes' => '8 personnes',
        ],
        'label' => false,
      ]);
    }
    $builder
    ->add('message', TextareaType::class, array(
      'attr' => array(
        'placeholder' => 'Message',
      ),
      'label' => false,
    ))

    ->add('rgpd', CheckboxType::class, array(
      'attr' => array(
        'class' => "rgpd",
      ),
      'label' => "J'accepte que mes données soient enregistrées. *",
      "mapped" => false,
      'constraints' => array(
        new NotBlank(),
      )
    ))
    ->add('submit', SubmitType::class, array(
      'label' => 'Envoyer'
    ));

  }
  function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'action' => null,
      'type' => 'contact',
      'data_class' => ContactRequest::class,
      'contactType' => null
    ]);
  }
}
