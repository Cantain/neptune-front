 $(document).ready(function () {
    //initialize swiper when document ready
    var mySwiper = new Swiper ('#swiper1', {
      // Optional parameters
      direction: 'horizontal',
      loop: true,
      autoplay: {
        delay : 5000,
      },
      observer: true, 
      observeParents: true,
      effect: 'fade',
      speed:1000,
      fadeEffect: {
      	crossFade: true
      },
      navigation: {
      	nextEl: '.swiper-button-next',
      	prevEl: '.swiper-button-prev',
      },
      on: {
        init:function(){
          const photos = this.$el[0].querySelectorAll('.photo:not(.loaded),.lazy');

          if($(".swiper-container2 .swiper-slide").length  <= 3){
            $('.swiper-wrapper').addClass( "disabled" );
            $('.swiper-pagination').addClass( "disabled" );
          }

          if(LazyLoad !== undefined){
            if(LazyLoad.ImageObserver != null){
             photos.forEach(function(photo){
              LazyLoad.ImageObserver.observe(photo);
            });
           }
           else{
            photos.forEach(function(photo){
              LazyLoad.lazyObjects[lazyImages.length] = photo;
            })
          }
        }
      }
    },
  })
  });