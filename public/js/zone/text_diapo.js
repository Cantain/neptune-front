 $(document).ready(function () {
    //initialize swiper when document ready
    var mySwiper = new Swiper ('#swiper2', {
      // Optional parameters
      direction: 'horizontal',
      loop: true,
      autoplay: {
        delay : 5000,
      },
      observer: true, 
      observeParents: true,
      speed:1000,
      navigation: {
      	nextEl: '#swiper-next2',
      	prevEl: '#swiper-prev2',
      },
      on: {
        init:function(){
          const photos = this.$el[0].querySelectorAll('.photo:not(.loaded),.lazy');

          if(LazyLoad !== undefined){
            if(LazyLoad.ImageObserver != null){
             photos.forEach(function(photo){
              LazyLoad.ImageObserver.observe(photo);
            });
           }
           else{
            photos.forEach(function(photo){
              LazyLoad.lazyObjects[lazyImages.length] = photo;
            })
          }
        }
      }
    },
  })
});