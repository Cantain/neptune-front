var ElementListAndDetails = (function(){
    var self = {};
    $('.zone.special-carou-3899e9bb864f8eac62a59e159db657cb68fd4bab').each(function(){
        var zone = $(this);
        var slider = zone.find('.main-swiper');

        if(slider.length){
            var swiperIt = new Swiper(slider[0], {
                direction: 'horizontal',
                loop: true,
                speed:500,
                slidesPerView: 1,
                spaceBetween: 30,
                centeredSlides: true,
                navigation: {
                    nextEl: '.swiper-button-next.diapo_rooms',
                    prevEl: '.swiper-button-prev.diapo_rooms',
                },
            });
        }

        var galleryThumbs = new Swiper(zone.find('.gallery-thumbs')[0], {
            spaceBetween: 40,
            slidesPerView: 3,
            loop: false,
            freeMode: true,
            loopedSlides: 5, //looped slides should be the same
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

        var swiper = new Swiper(zone.find('.gallery-top')[0], {
            spaceBetween: 10,
            loop:false,
            loopedSlides: 5, //looped slides should be the same
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: galleryThumbs,
            },

        });
    });


    var lazyInit = function()
    {
        $('.swiper-container').find('.photo').each(function () {
            if(LazyLoad.ImageObserver != null){
                LazyLoad.ImageObserver.observe($(this)[0]);
            }else{
                LazyLoad.lazyObjects[LazyLoad.lazyObjects.length] = $(this)[0];
            }
        });
    };
    lazyInit();
    return self;
})();